---
- name: Remove old certbot package (Debian)
  apt:
    name: certbot
    state: absent
    purge: true
  when: ansible_facts['os_family'] == "Debian"

- name: Remove old certbot package (RHEL)
  yum:
    name: certbot
    state: absent
  when: ansible_facts['os_family'] == 'RedHat'

- name: Remove old certbot package files (RHEL)
  file:
    path: /etc/letsencrypt/
    state: absent
  when: ansible_facts['os_family'] == 'RedHat'

- name: Remove any old SSL Certificates
  file:
    path: "{{ item }}"
    state: absent
  loop:
    - "/opt/haproxy/{{ external_host }}.pem"
    - "/opt/haproxy/{{ external_host }}.pem.key"

- name: Install Socat for acme.sh
  package:
    name: socat

- name: Install and configure Cronie (Amazon Linux 2023)
  shell: yum install -y cronie && systemctl enable crond && systemctl start crond
  when:
    - ansible_facts['distribution'] == "Amazon"
    - ansible_facts['distribution_major_version'] != '2'

- name: Check if acme.sh is already installed
  stat:
    path: /opt/acme.sh/acme.sh
  register: acme_sh_file

- name: Install acme.sh
  shell: curl https://raw.githubusercontent.com/acmesh-official/acme.sh/master/acme.sh | sh -s -- --install-online --home /opt/acme.sh --accountemail {{ external_ssl_letsencrypt_issuer_email }}
  when: not acme_sh_file.stat.exists

- name: Upgrade acme.sh
  command: /opt/acme.sh/acme.sh --upgrade

- name: Run acme.sh to generate certs
  command: /opt/acme.sh/acme.sh --issue --standalone --httpport 88 --server {{ acme_sh_server }} -d {{ external_host }} {{ ('-d ' + container_registry_external_host) if container_registry_enable else '' }} --key-file /opt/haproxy/ssl/{{ external_host }}.pem.key --fullchain-file /opt/haproxy/ssl/{{ external_host }}.pem --reloadcmd 'docker kill -s HUP haproxy'
  register: acme_sh_cmd
  failed_when: acme_sh_cmd.rc == 1

- name: Update cert files ownership
  file:
    path: "{{ item }}"
    owner: '99' # HAProxy Docker User ID
    mode: '0600'
  loop:
    - "/opt/haproxy/ssl/{{ external_host }}.pem"
    - "/opt/haproxy/ssl/{{ external_host }}.pem.key"

- name: Reload HAProxy
  command: docker kill -s HUP haproxy
